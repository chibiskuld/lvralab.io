---
weight: 900
title: Unity
---

# Unity

For the best experience, we found that running Unity from [Flatpak](https://flathub.org/apps/com.unity.UnityHub) is usually preferred.

You have two options for setting up VRChat avatar/world projects:
1. vrc-get CLI, a native FOSS package manager written in Rust
2. LinuxCreatorCompanion GUI, which runs VRChat Creator Companion with Linux-specific patches

## Option 1. vrc-get

First, take a look at this page from [the official VRChat docs](https://creators.vrchat.com/sdk/upgrade/unity-2022/#using-the-unity-website) about the Unity 2022 upgrade. We'll do something similar.

1. Install Unity Hub from Flatpak.
1. Install the 3rd party VRChat package manager [`vrc-get`](https://github.com/anatawa12/vrc-get).
   - You can install via `cargo install vrc-get`.
   - Or, install this AUR package: [`vrc-get` AUR](https://aur.archlinux.org/packages/vrc-get).
   - Or, you can run the binary from the releases page.
1. Open Unity Hub and sign in with your Unity account.
1. Install **2022.3.6f1** (the current supported Unity version, at the time of writing) by running `xdg-open unityhub://2022.3.6f1/b9e6e7e9fa2d`.
1. When prompted, select *Android Build Support* and *Windows Build Support (Mono)*.
1. Create a new project with the **3D Core template**, making sure to name it and give it a better location than `$HOME`.
1. It may open windows partially offscreen. If this happens, see the 
1. Close the Unity project (will probably crash later if you don't, no big deal but still).
1. Open terminal and `cd` to the project dir.
1. Run either `vrc-get install com.vrchat.avatars` or `vrc-get install com.vrchat.worlds` depending on what you will be making.
1. Reopen the Unity project and it will import the new packages.
1. Go to VRChat SDK -> Show Control Panel and dock it to the main window.

Now you should be able to create and upload avatars and worlds like you normally would.

If you're having issues, see the [Troubleshooting](#troubleshooting) section.

## Option 2. LinuxCreatorCompanion

If you prefer GUIs, it is now possible to use VRChat Creator Companion (VCC) on Linux.

The official VRChat Creator Companion uses Webview2 (Microsoft Edge). This component is difficult to install, and doesn't run natively. Instead, you can use LinuxCreatorCompanion.

[GitHub repo for LinuxCreatorCompanion](https://github.com/RinLovesYou/LinuxCreatorCompanion)

It works by using wine to extract the necessary bits to re-embed into a native client application. While experimental, you can enjoy much easier avatar, world, and asset creation. Again, this is experimental, and we discourage you from trying this on important projects. **Remember to make frequent project backups.** Please leave any feedback about bugs or ideas on the GitHub issues tab.

To install, click the releases tab, download the zip, extract it somewhere, and run `./LinuxCreatorCompanion`.

**Note:** If you're using Unity Hub via flatpak, it will have trouble finding and opening Unity. For now, create this wrapper script, chmod +x it, and put it somewhere in your PATH, like `/usr/local/bin/unityhub`.

```bash
#!/usr/bin/env bash
exec flatpak run com.unity.UnityHub "$@"
```

## Troubleshooting

### AssetBundle was not built

When building and uploading, this error message may appear. It's caused by VRCSDK building an AssetBundle whose filename has mixed caps, when Unity AssetBundles are really only supposed to have lowercase filenames.

To fix this, import `WorldSDKPatch.unitypackage`: https://github.com/thegu5/VRCSDKonLinux/releases


### Shaders fail to include some .cginc files

This can happen with Poiyomi. It's caused by case-sensitivity. You may need to edit the shader files directly. Open them in any text editor, and correct the casing of filenames.

### Editor windows appear partially offscreen

Unity may open undocked panel windows (such as color pickers and unitypackage import dialogs) partially offscreen.

Some workarounds:
- For KDE, hold the Super key and click anywhere in the window to drag it back onscreen.
- For KDE, press ALT+F3 -> More Actions -> Move.

### Drag-and-drop does not work

Rather than using Assets -> Import New Asset or Import Package, it's normally possible to drag-and-drop a unitypackage or other asset from a file browser directly into Unity's Project pane, where it will be copied to the Assets folder.

However, if your file browser is running on Wayland, this will show "Failed copying file" errors.

See issue [flathub/com.unity.UnityHub#97](https://github.com/flathub/com.unity.UnityHub/issues/97).

As a workaround, launch your file browser in Xwayland mode. Unity will accept the file as it should.

```
WAYLAND_DISPLAY="" dolphin
```
