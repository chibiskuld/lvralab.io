---
weight: 300
title: WlxOverlay-S
---

# WlxOverlay-S

- [WlxOverlay-S GitHub repository](https://github.com/galister/wlx-overlay-s)

WlxOverlay-S is a tool that lets users interact with their X11 or Wayland desktop from inside VR.

It supports a vast variety of desktop environments, and comes with a fully customizable keyboard and watch.

Please take a look at the GitHub Readme for a comprehensive guide.

## Support

Reach out in the `wlxoverlay` room in Discord or Matrix.