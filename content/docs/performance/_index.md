---
title: Performance
weight: 250
---

# Performance

## Set AMD GPU power profile mode

This is important to avoid stuttering.

### The simple way: Use CoreCtrl

- Install [CoreCtrl](https://gitlab.com/corectrl/corectrl)
- Select your GPU on the top
- Set Performance mode to Advanced
- Set Power profile to VR
- Set the GPU and Memory sliders to max

### Enable VR profile using a script

```bash
#!/usr/bin/env bash

# Enable manual override
echo "manual" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level

# Translate "VR" into profile number
vr_profile=$(cat /sys/class/drm/card0/device/pp_power_profile_mode | grep ' VR ' | awk '{ print $1; }')

# Set profile to VR
echo $vr_profile | sudo tee /sys/class/drm/card0/device/pp_power_profile_mode
```

### Disable VR profile using a script
```bash
#!/usr/bin/env bash

# Disable manual override
echo "auto" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level

# Set profile to DEFAULT
echo 0 | sudo tee /sys/class/drm/card0/device/pp_power_profile_mode
```
