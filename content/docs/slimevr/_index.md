---
title: SlimeVR
weight: 250
---

# SlimeVR

- [SlimeVR Documentation](https://docs.slimevr.dev/)
- [SlimeVR on Linux guide](https://docs.slimevr.dev/tools/linux-installation.html)

> SlimeVR is a set of open hardware sensors and open source software that facilitates full-body tracking (FBT) in virtual reality.

SlimeVR is an open source IMU based full body tracking system. Being open source it has Linux support and there are a few variants you can either buy or build yourself.

You can pre-order the official SlimeVR trackers from [their crowdsupply page](https://www.crowdsupply.com/slimevr/slimevr-full-body-tracker), but at the time of writing this if you order them right now you'd need to wait approximately 4 months for your orders to ship, due to the very high demand driven by low cost and good quality tracking compared to other solutions.

## Assembling

Alternatively you can build it on your own, and you have a few variants to choose from:

- [Official SlimeVR DIY builders guide](https://docs.slimevr.dev/diy/index.html)
- [Frozen Slimes V2](https://github.com/frosty6742/frozen-slimes-v2)
  - while a bit bigger, Frozen Slimes V2 tend to be cheaper and a lot easier to build, being approachable even for people that are inexperienced with soldering

It's generally **highly recommended** to opt for the more expensive but higher quality [BNO085](https://shop.slimevr.dev/products/slimevr-imu-module-bno085) IMU module. This should offer the highest quality tracking with best precision and minimal drifting.

## Installing SlimeVR Server

<!-- will likely need to move this somewhere else -->

- [AppImage](https://github.com/SlimeVR/SlimeVR-Server/releases/latest/download/SlimeVR-amd64.appimage)
- [Flathub](https://flathub.org/apps/dev.slimevr.SlimeVR)

## Selecting your variant

There are currently two ways to use SlimeVR:

- Via SteamVR driver
- Directly via VRChat OSC (Open Sound Control)

## First method: SteamVR driver

Download the [SlimeVR driver for SteamVR](https://github.com/SlimeVR/SlimeVR-OpenVR-Driver/releases/latest/download/slimevr-openvr-driver-x64-linux.zip), extract it and place the `slimevr` folder in the `$HOME/.steam/steam/steamapps/common/SteamVR/drivers/` folder. This path may change depending on where your SteamVR install is located. To make sure you moved the right folder, make sure that the following path exists: `$HOME/.steam/steam/steamapps/common/SteamVR/drivers/slimevr/bin/linux64/driver_slimevr.so`.

Then you'll need to register the driver using the following command.

**Important**: make sure to **NOT** run this command twice, adding a driver twice will cause problems.

```bash
$HOME/.steam/steam/steamapps/common/SteamVR/bin/linux64/vrpathreg.sh adddriver $HOME/.steam/steam/steamapps/common/SteamVR/drivers/slimevr
```

Now the only thing you have to do is restart the SlimeVR server.

## Second method: VRChat OSC

**This method is only available in VRChat.**

It requires fewer steps to configure and allows the use of the full FOSS stack without using SteamVR. For now, it is less accurate due to OSC limitations. [More information here.](https://docs.slimevr.dev/server/osc-information.html)

- Step 1: Tick the checkbox in SlimeVR settings -> VRChat OSC Trackers -> Enable

![Configuration steps of VRChat OSC](vrchat_osc.png "Configuring VRChat OSC")

- Step 2: Start VRChat

- Step 3: Go to the radial menu, and enable OSC:

- Step 4: Calibrate your trackers via built-in SlimeVR configuration wizard (Mounting and body proportions)

<!-- Image resourced from https://docs.slimevr.dev/server/osc-information.html -->

![How to enable OSC in Radial Menu](radial_menu.png)

- Step 5: Press the "Full Reset" button while standing straight

![Full reset button in SlimeVR menu](full_reset.png)

- Step 6: Go to VRChat settings and press "Calibrate FBT" in "Quick actions".

!["Calibrate FBT" button in VRChat Quick Actions menu](quick_actions.png)

- Step 7: [T-pose](https://en.wikipedia.org/wiki/T-pose) yourself and submit your calibration via pressing triggers on both sides.

Done! From now on, you should be able to freely move your tracked limbs.
